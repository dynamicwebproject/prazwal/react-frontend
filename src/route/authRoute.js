import React from "react";
import { Redirect, Route } from "react-router-dom";
import { useCheckLogger } from "../components/hooks/useCheckLogger";

import "./routeStyles.css";

const AuthRoute = ({ component: Component, ...data }) => {
  const { isAdmin } = useCheckLogger();
  return (
    <>
      <Route
        {...data}
        render={(props) => {
          return isAdmin ? (
            <Redirect exact to="/administration/" />
          ) : (
            <div className="adminAuthWrapper d-flex align-items-center">
              <div className="adminAuthContent row container mx-auto h-50">
                <div className="adminAuthComponent col-6 d-flex">
                  <div className="adminAuthHeader row w-50 mx-auto">
                    <img src={"/Images/companylogo.png"} alt="r n b traders" />{" "}
                    R n B TRADE
                  </div>
                  <Component {...props} />
                </div>
                <div className="adminAuthImage col-6 d-flex justify-content-end">
                  <img src={"/Images/nonauthimage.png"} alt="r n b traders" />
                </div>
              </div>
            </div>
          );
        }}
      />
    </>
  );
};

export default AuthRoute;
