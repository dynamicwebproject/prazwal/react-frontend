import React from "react";
import { Link, Redirect, Route, Switch } from "react-router-dom";

import NormalRoute from "./normalRoute";
import AdminRoute from "./adminRoute";

import ForgotPassword from "../pages/auth/forgotPassword";
import ResetPassword from "../pages/auth/resetPassword";

import LandingPage from "../pages/landingPage";
import AboutPage from "../pages/about";
import CartPage from "../pages/cart";
import CustomerCarePage from "../pages/customerCare";
import ProductPage from "../pages/product/index";

import ProductByCategoryPage from "../pages/productByCategory";
import AuthRoute from "./authRoute";
import { AdminLogin } from "../pages/auth/adminLogin";
import {
  AddProduct,
  AdminViewProduct,
  UdpateProduct,
  AdminDashboard,
  AdminViewCustomer,
} from "../pages/admin";
import { useCheckLogger } from "../components/hooks/useCheckLogger";

const AppRouting = () => {
  const { isAdmin } = useCheckLogger();
  function NotFound() {
    return (
      <>
        <h1>Error 404!</h1>

        <h5>Page Not Found.</h5>
        <Link to={isAdmin ? "/administration/" : "/"}>Back to home</Link>
      </>
    );
  }

  return (
    <>
      <Switch>
        {/*UNPROTECTED ROUTE*/}
        <NormalRoute exact path="/" component={LandingPage} />
        <NormalRoute exact path="/about" component={AboutPage} />
        <NormalRoute exact path="/cart" component={CartPage} />
        <NormalRoute exact path="/customerCare" component={CustomerCarePage} />
        <NormalRoute
          exact
          path="/product/:category"
          component={ProductByCategoryPage}
        />
        <NormalRoute
          exact
          path="/product/:category/:productId"
          component={ProductPage}
        />

        <AuthRoute exact path="/forgot-password" component={ForgotPassword} />
        <AuthRoute
          exact
          path="/reset-password/:token"
          component={ResetPassword}
        />

        <AuthRoute exact path="/administration/login" component={AdminLogin} />

        {/*ADMIN ROUTES*/}
        <AdminRoute exact path="/administration/" component={AdminDashboard} />
        <AdminRoute
          exact
          path="/administration/products/view"
          component={AdminViewProduct}
        />
        <AdminRoute
          exact
          path="/administration/products/add"
          component={AddProduct}
        />
        <AdminRoute
          exact
          path="/administration/products/update"
          component={UdpateProduct}
        />
        <AdminRoute
          exact
          path="/administration/products/update/:productId"
          component={UdpateProduct}
        />
        <AdminRoute
          exact
          path="/administration/customers"
          component={AdminViewCustomer}
        />
        <AdminRoute
          exact
          path="/administration/orders"
          component={AdminViewProduct}
        />

        <Route component={NotFound} />
      </Switch>
    </>
  );
};

export default AppRouting;
