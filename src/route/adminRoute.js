import React from "react";
import { Route, Redirect } from "react-router-dom";
import { LogoutModalBox } from "../components/coreUI/Modal/PredefineModals";
import AdminSideNavigation from "../components/coreUI/sidebar";
import { useCheckLogger } from "../components/hooks/useCheckLogger";

const AdminRoute = ({ component: Component, ...data }) => {
  const { isAdmin } = useCheckLogger();

  return (
    <>
      <Route
        {...data}
        render={(props) => {
          return localStorage.getItem("token") && isAdmin ? (
            <div className="row">
              <div className="sidenav col-lg-3 col-sm-12">
                <AdminSideNavigation />
              </div>
              <LogoutModalBox />

              <div className="mx-auto my-5 text-left py-4 col-lg-8 col-sm-11">
                <Component {...props} state={data.state} />
              </div>
              {/* ADD FOOTER */}
            </div>
          ) : (
            <>
              <Redirect to="/" />
            </>
          );
        }}
      />
    </>
  );
};

export default AdminRoute;
