import React from "react";
import { Route } from "react-router-dom";
import NavBar from "../components/coreUI/NavBar";
import Footer from "../components/Footer";

const NormalRoute = ({ component: Component, ...data }) => {
  return (
    <>
      <Route
        {...data}
        render={(props) => {
          return (
            <>
              <NavBar />
              <div className="normalContentWrapper">
                <Component {...props} />
              </div>
              <Footer />
            </>
          );
        }}
      />
    </>
  );
};

export default NormalRoute;
