const LoginFormTypes = {
  username: "username",
  password: "password",
};

const UserTypes = {
  ADMIN: 'admin',
  CUSTOMER: 'customer',
}

export { LoginFormTypes, UserTypes };
