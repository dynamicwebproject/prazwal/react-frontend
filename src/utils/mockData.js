const coverImages = [
  {
    src: "/Images/coverone.jpg",
    alt: "cover one",
    label: "Image of cover one",
    description:
      "lorem ipsum one lorem ipsum one lorem ipsum one lorem ipsum one lorem ipsum one ",
  },
  {
    src: "/Images/covertwo.jpg",
    alt: "cover two",
    label: "Image of cover two",
    description:
      "lorem ipsum two lorem ipsum two lorem ipsum two lorem ipsum two lorem ipsum two ",
  },
  {
    src: "/Images/coverthree.jpeg",
    alt: "cover three",
    label: "Image of cover three",
    description:
      "lorem ipsum three lorem ipsum three lorem ipsum three lorem ipsum three lorem ipsum three ",
  },
];

const allProducts = [
  {
    productId: "s09dfuio13103on23",
    name: "Product one",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    status: "NEW",
    img: "/Images/products/doorone.jpg",
    alt: "door",
    category: "door",
    quantity: 100,
    price: 1000,
    colors: ["red", "green", "blue"],
    sizes: null,
  },
  {
    productId: "sdfioj230923j23kl",
    name: "Product two",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    status: "NEW",
    img: "/Images/products/doorone.jpg",
    alt: "door",
    category: "window",
    quantity: 100,
    price: 1000,
    colors: null,
    sizes: ["small", "medium", "large"],
  },
  {
    productId: "0923iosdniospo232",
    name: "Product three",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    status: "NEW",
    img: "/Images/products/doorone.jpg",
    alt: "door",
    category: "door",
    quantity: 100,
    price: 1000,
    colors: ["red", "green", "blue"],
    sizes: ["small", "medium", "large"],
  },
  {
    productId: "09sio3niososef093",
    name: "Product four",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    status: "SALE",
    img: "/Images/products/doorone.jpg",
    alt: "door",
    category: "door",
    quantity: 100,
    price: 1000,
    colors: ["red", "green", "blue"],
    sizes: ["small", "medium", "large"],
  },
  {
    productId: "09sij3rnio093u334i",
    name: "Product five",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    status: "OUT",
    img: "/Images/products/doorone.jpg",
    alt: "door",
    category: "window",
    quantity: 100,
    price: 1000,
    colors: ["red", "green", "blue"],
    sizes: ["small", "medium", "large"],
  },
  {
    productId: "2408hsdoi23rio23io",
    name: "Product seven",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    img: "/Images/products/doorone.jpg",
    alt: "door",
    category: "window",
    quantity: 100,
    price: 1000,
    colors: ["red", "green", "blue"],
    sizes: ["small", "medium", "large"],
  },
  {
    productId: "9ewinl23iorsde9209",
    name: "Product eight",
    description:
      "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
    status: "SALE",
    img: "/Images/products/doorone.jpg",
    alt: "door",
    category: "door",
    quantity: 100,
    price: 1000,
    colors: ["red", "green", "blue"],
    sizes: ["small", "medium", "large"],
  },
];


export { allProducts, coverImages };
