import passwordStrength from './passwordStrength'

export const validateEmail = (email) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

export const validatePassword = (password) => {
  if (!password) {
    return
  }
  return (Object.keys(passwordStrength)).find(k => {
    return (new RegExp(passwordStrength[k])).test(password)
  })
}


