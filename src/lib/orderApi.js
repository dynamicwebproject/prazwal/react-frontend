import sendRequest from "./sendRequest";

const BASE_PATH = "/order";

const requestPlaceOrder = async (orderDetail) => {
  let formData = new FormData();
  Object.keys(orderDetail).forEach((pd) => {
    formData.set(pd, orderDetail[pd]);
  });

  const requestOptions = {
    method: "POST",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestGetAllOrders = async () => {
  const requestOptions = {
    method: "GET",
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestDeleteOrder = async (id) => {
  const requestOptions = {
    method: "GET",
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

const requestChangeOrderStatus = async (id, status) => {
  const formData = new FormData();
  formData.set("status", status);
  const requestOptions = {
    method: "PUT",
    data: formData,
  };
  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

export {
  requestPlaceOrder,
  requestGetAllOrders,
  requestChangeOrderStatus,
  requestDeleteOrder,
};
