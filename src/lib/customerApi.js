import sendRequest from "./sendRequest";
const BASE_PATH = "/customer";

const requestGetAllCustomers = async () => {
  const requestOptions = {
    method: "GET",
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestAddUser = async (userDetail) => {
  const formData = new FormData();
  Object.keys(userDetail).map((key) => {
    formData.set(key, userDetail[key]);
  });
  const requestOptions = {
    method: "POST",
    secured: false,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/create`, requestOptions);
  return res;
};

const requestUpdateUser = async (userDetail, id) => {
  const formData = new FormData();
  Object.keys(userDetail).map((key) => {
    formData.set(key, userDetail[key]);
  });
  const requestOptions = {
    method: "PUT",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}/`, requestOptions);
  return res;
};

const requestDeleteUser = async (username) => {
  const formData = new FormData();
  formData.set("username", username);
  const requestOptions = {
    method: "DELETE",
    secured: true,
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/delete/`, requestOptions);
  return res;
};
export { requestGetAllCustomers, requestAddUser, requestDeleteUser, requestUpdateUser };
