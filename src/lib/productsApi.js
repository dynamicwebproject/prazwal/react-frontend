import sendRequest from "./sendRequest";

const BASE_PATH = "/product";

const requestAddProduct = async (productDetail) => {
  let formData = new FormData();
  Object.keys(productDetail).forEach((pd) => {
    formData.set(pd, productDetail[pd]);
  });

  const requestOptions = {
    method: "POST",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};

const requestGetAllProducts = async () => {
  const requestOptions = {
    method: "GET",
  };

  const res = await sendRequest(`${BASE_PATH}/`, requestOptions);
  return res;
};


const requestUpdateProduct = async (productDetail, id) => {
  let formData = new FormData();
  Object.keys(productDetail).forEach((pd) => {
    formData.set(pd, productDetail[pd]);
  });

  const requestOptions = {
    method: "PUT",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/${id}`, requestOptions);
  return res;
};

const requestDeleteProduct = async (id) => {
  const requestOptions = {
    method: "DELETE",
  };

  const res = await sendRequest(`${BASE_PATH}/${id}`, requestOptions);
  return res;
};

export {
  requestAddProduct,
  requestGetAllProducts,
  requestUpdateProduct,
  requestDeleteProduct,
};
