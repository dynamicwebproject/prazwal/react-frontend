import sendRequest from "./sendRequest";

const BASE_PATH_CATEGORY = "/category";
const BASE_PATH_SUB_CATEGORY = "/subcategory";

const requestAddCategory = async (categoryDetail) => {
  let formData = new FormData();
  Object.keys(categoryDetail).forEach((pd) => {
    formData.set(pd, categoryDetail[pd]);
  });

  const requestOptions = {
    method: "POST",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH_CATEGORY}/`, requestOptions);
  return res;
};

const requestGetAllCategories = async () => {
  const requestOptions = {
    method: "GET",
  };

  const res = await sendRequest(`${BASE_PATH_CATEGORY}/`, requestOptions);
  return res;
};

const requestUpdateCategory = async (categoryDetail, id) => {
  let formData = new FormData();
  Object.keys(categoryDetail).forEach((pd) => {
    formData.set(pd, categoryDetail[pd]);
  });

  const requestOptions = {
    method: "PUT",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH_CATEGORY}/${id}`, requestOptions);
  return res;
};

const requestDeleteCategory = async (id) => {
  const requestOptions = {
    method: "DELETE",
  };

  const res = await sendRequest(`${BASE_PATH_CATEGORY}/${id}`, requestOptions);
  return res;
};

const requestAddSubCategory = async (subCategoryDetail) => {
  let formData = new FormData();
  Object.keys(subCategoryDetail).forEach((pd) => {
    formData.set(pd, subCategoryDetail[pd]);
  });

  const requestOptions = {
    method: "POST",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH_SUB_CATEGORY}/`, requestOptions);
  return res;
};

const requestGetAllSubCategories = async () => {
  const requestOptions = {
    method: "GET",
  };

  const res = await sendRequest(`${BASE_PATH_SUB_CATEGORY}/`, requestOptions);
  return res;
};

const requestUpdateSubCategory = async (subCategoryDetail, id) => {
  let formData = new FormData();
  Object.keys(subCategoryDetail).forEach((pd) => {
    formData.set(pd, subCategoryDetail[pd]);
  });

  const requestOptions = {
    method: "PUT",
    data: formData,
  };

  const res = await sendRequest(
    `${BASE_PATH_SUB_CATEGORY}/${id}`,
    requestOptions
  );
  return res;
};

const requestDeleteSubCategory = async (id) => {
  const requestOptions = {
    method: "DELETE",
  };

  const res = await sendRequest(
    `${BASE_PATH_SUB_CATEGORY}/${id}`,
    requestOptions
  );
  return res;
};

export {
  requestAddCategory,
  requestGetAllCategories,
  requestUpdateCategory,
  requestDeleteCategory,
  requestAddSubCategory,
  requestGetAllSubCategories,
  requestUpdateSubCategory,
  requestDeleteSubCategory,
};
