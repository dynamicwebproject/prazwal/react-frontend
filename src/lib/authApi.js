import sendRequest from "./sendRequest";
import CryptoJS from "crypto-js";

import { crypto_secret } from "../utils/constants";

const BASE_PATH = "/auth";

const requestLogin = async ({ username, password }) => {
  const formData = new FormData();
  formData.set("username", username);
  formData.set("password", password);

  const requestOptions = {
    method: "POST",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/login/`, requestOptions);
  console.log(res);
  if (res && !res.error) {
    localStorage.setItem("token", res?.token?.access);
    localStorage.setItem(
      "logger",
      CryptoJS.AES.encrypt(res.is_staff.toString(), crypto_secret)
    );
  }
  return res;
};

const requestSendResetPasswordMail = async (email) => {
  const formData = new FormData();
  formData.set("email", email);

  const requestOptions = {
    method: "POST",
    data: formData,
  };

  const res = await sendRequest(
    `${BASE_PATH}/password_reset/`,
    requestOptions
  );
  return res;
};

const requestResetPassword = async ({ token, email }) => {
  const formData = new FormData();
  formData.set("token", token);
  formData.set("email", email);

  const requestOptions = {
    method: "POST",
    data: formData,
  };

  const res = await sendRequest(`${BASE_PATH}/password_reset/confirm/`, requestOptions);
  return res;
};

export { requestLogin, requestSendResetPasswordMail, requestResetPassword };
