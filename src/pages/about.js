import React from "react";

const AboutPage = () => {
  return (
    <>
      <div className="aboutContainer">
        <h1 className="pageHeadings">About RnB</h1>
        <div className="rowColWrapper">
          <div className="row rowColRow">
            <div className="col-6 rowColCol colLeft">
              <img
                src={"/Images/services-images/serviceOS.svg"}
                alt="onlineShopping.svg"
              />
            </div>
            <div className="col-6 rowColCol colRight">
              <h3>Shopping Online</h3>
              <p>
                The purpose of online shopping is to save time, save money.
                Through online shopping one can save his valuable time. One can
                watch and select things he wants to buy. Through online shopping
                we can save our money because prices are less than market prices
                and we receive our bought things at our home.
              </p>
            </div>
          </div>
          <div className="row rowColRow">
            <div className="col-6 rowColCol colLeft">
              <h3>Product Cart and Details</h3>
              <p>
                A shopping cart on an online retailer's site is a piece of
                software that facilitates the purchase of a product or service.
                It accepts the customer's payment and organizes the distribution
                of that information to the merchant, payment processor and other
                parties.
              </p>
            </div>
            <div className="col-6 rowColCol colRight">
              <img
                src={"/Images/services-images/serviceOP.svg"}
                alt="onlinePurchase.svg"
              />
            </div>
          </div>
          <div className="row rowColRow">
            <div className="col-6 rowColCol colLeft">
              <img
                src={"/Images/services-images/serviceOSP.svg"}
                alt="onlineShoppingPhone.svg"
              />
            </div>
            <div className="col-6 rowColCol colRight">
              <h3>Order using Phone/Mail</h3>
              <p>
                Mail/phone order transaction. A mail or phone order transaction
                is one in which a cardholder orders goods or services from a
                merchant by telephone, mail or other means of telecommunication,
                and where neither the card nor the cardholder is present at the
                merchant outlet.
              </p>
            </div>
          </div>
          <div className="row rowColRow">
            <div className="col-6 rowColCol colLeft">
              <h3>Delivery Service</h3>
              <p>
                elivery is the process of transporting goods from a source
                location to a predefined destination. There are different
                delivery types. ... Postal, courier, and relocation services
                also deliver goods for commercial and private interests.
              </p>
            </div>
            <div className="col-6 rowColCol colRight">
              <img
                src={"/Images/services-images/serviceDL.svg"}
                alt="agriculture.svg"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AboutPage;
