import React from "react";
import Contact from "../components/contact";
import FAQ from "../components/faq";

const CustomerCare = () => {
  return (
    <div className="customerCareWrapper">
      <FAQ />
      <Contact />
    </div>
  );
};

export default CustomerCare;
