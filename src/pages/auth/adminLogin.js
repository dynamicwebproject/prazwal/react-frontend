import React from "react";
import { Link } from "react-router-dom";
import LoginForm from "../../components/coreUI/Login/login";

export const AdminLogin = () => {
  return (
    <>
      <h1>Login to your account</h1>
      <LoginForm />
      <div>
        <Link to="/forgot-password">FORGOT PASSWORD?</Link>
      </div>
    </>
  );
};
