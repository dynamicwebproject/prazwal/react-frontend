import React, { useState } from "react";
import { toast } from "react-toastify";
import { Input } from "../../components/coreUI";
import { requestSendResetPasswordMail } from "../../lib/authApi";
import { errors } from "../../utils/constants";

const ForgotPassword = (props) => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");

  const handleChange = (e) => {
    const { value } = e.target;
    setEmail(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await requestSendResetPasswordMail(email);
    if (!response || response.error) {
      setError(response.error || errors.generic);
    } else {
      toast.success("Reset link sent to email.");
      props.history.push("/login");
    }
  };

  const disableSubmitButton = () => !email || error;

  return (
    <>
      <h1>Forgot Password</h1>
      <form onSubmit={handleSubmit}>
        <Input
          type="email"
          name="email"
          handleChange={handleChange}
          value={email}
          error={error}
        />
        <button type="submit" disabled={disableSubmitButton()}>
          Confirm
        </button>
      </form>
    </>
  );
};

export default ForgotPassword;
