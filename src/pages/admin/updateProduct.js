import React, { useContext, useEffect, useState } from "react";
import { AddOrUpdateForm } from "../../components/admin/Product/form";
import { SelectOption } from "../../components/coreUI";
import { ProductContext } from "../../components/hooks/useProductContext";

export const UdpateProduct = (props) => {
  const { productList, categoryList } = useContext(ProductContext);
  const [toEditProduct, setToEditProduct] = useState(null);

  const paramsProductId = props.match.params.productId;

  useEffect(() => {
    if (!!paramsProductId) {
      setToEditProduct(
        productList.filter((pl) => pl.id === parseInt(paramsProductId))[0]
      );
    }
  }, [paramsProductId]);

  const category = (id) =>
    categoryList.filter((cl) => cl.id === id)[0].category_name;

  const selectOptions = productList.map((pdl) => {
    return {
      key: pdl.id,
      value: pdl.id,
      displayName: `${pdl.name} - ${category(pdl.category)}`,
    };
  });

  const handleChange = (e) => {
    const { value } = e.target;
    setToEditProduct(productList.filter((pl) => pl.id === parseInt(value))[0]);
  };

  const handleSuccess = () =>
    props.history.push("/administration/products/view");

  return (
    <>
      <h1 className="text-center text-muted">
        Update{" "}
        <span className="text-primary">{` '${
          toEditProduct?.name || ""
        }' `}</span>
      </h1>
      {(!paramsProductId || paramsProductId < 0) && (
        <SelectOption
          label="Select product to edit"
          placeholder="Product"
          options={selectOptions}
          value={toEditProduct}
          handleChange={handleChange}
          wrapperClass="d-block mx-auto "
        />
      )}
      <AddOrUpdateForm updateData={toEditProduct} onSuccess={handleSuccess} />
    </>
  );
};
