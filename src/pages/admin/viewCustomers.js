import React, { useEffect, useState } from "react";
import { requestGetAllCustomers } from "../../lib/customerApi";

export const AdminViewCustomer = () => {
  const [customerList, setCustomerList] = useState([]);

  useEffect(() => {
    fetchAllCustomers();
  }, []);

  const fetchAllCustomers = async () => {
    const res = await requestGetAllCustomers();
    if (res && !res?.error) {
      setCustomerList(res.data);
    }
  };

  return (
    <>
      <>
        <h2>CUSTOMERS</h2>
        <table className="table m-0" style={{ "background-color": "white" }}>
          <thead className="thead-dark">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">username</th>
            </tr>
          </thead>
          <tbody>
            {customerList && customerList.length > 0 ? (
              customerList.map((cust) => (
                <tr>
                  <td>
                    {cust.first_name} {cust.last_name}
                    </td>
                  <td>{cust.email}</td>
                  <td>{cust.username}</td>
                </tr>
              ))
            ) : (
              <div>No data in table</div>
            )}
          </tbody>
        </table>
      </>
    </>
  );
};
