import React, { useContext, useState } from "react";
import { groupBy } from "lodash";
import { Link } from "react-router-dom";
import { ProductContext } from "../../components/hooks/useProductContext";
import { requestDeleteProduct } from "../../lib/productsApi";
import { toast } from "react-toastify";
import { ConfirmModalBox } from "../../components/coreUI/Modal/PredefineModals";

export const AdminViewProduct = () => {
  const {
    productList,
    categoryList,
    subCategoryList,
    setProductList,
  } = useContext(ProductContext);
  const groupByCategory = groupBy(productList, "category");

  const [toDeleteId, setToDeleteId] = useState(-1);

  const getCategory = (id) =>
    categoryList.filter((cl) => cl.id === id)[0]?.category_name;
  const getSubCategory = (id) =>
    subCategoryList.filter((scl) => scl.id === id)[0]?.subcategory_name;

  const handleDelete = async (val) => {
    if (val) {
      const res = await requestDeleteProduct(toDeleteId);
      if (res && !res?.error) {
        toast.success("Product deleted");
        setProductList();
      }
    }
  };

  return (
    <>
      {categoryList &&
        categoryList.length > 0 &&
        categoryList.map((cat) => (
          <>
            <h2>{cat.category_name.toUpperCase()}</h2>
            <table
              className="table m-0"
              style={{ "background-color": "white" }}
            >
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Image</th>
                  <th scope="col">Name</th>
                  <th scope="col" className="mw-50">
                    Category
                  </th>
                  <th scope="col">Sub Category</th>
                  <th scope="col" className="mw-50">
                    Brand
                  </th>
                  <th scope="col">Quantity</th>
                  <th scope="col">Price</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                {groupByCategory &&
                  groupByCategory[cat.id] &&
                  groupByCategory[cat.id].length > 0 &&
                  groupByCategory[cat.id].map((val) => (
                    <tr>
                      <td>
                        <img
                          src={val.image || "/Images/companylogo.png"}
                          alt={val.name}
                          height="40px"
                          width="40px"
                          className="img-fluid"
                          style={{ height: "50px", width: "50px" }}
                        />
                      </td>
                      <td>{val.name}</td>
                      <td>{getCategory(val.category)}</td>
                      <td className="econTableDesc">
                        {getSubCategory(val.id)}
                      </td>
                      <td>{val.brand}</td>
                      <td>{val.quantity}</td>
                      <td>{val.price}</td>
                      <td>
                        <Link
                          role="button"
                          className="btn btn-sm btn-outline-info"
                          to={`/administration/products/update/${val.id}`}
                        >
                          Edit
                        </Link>
                        <button
                          type="button"
                          className="btn btn-sm btn-outline-danger"
                          onClick={() => setToDeleteId(val.id)}
                          data-toggle="modal"
                          data-target="#confirmation"
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
            <ConfirmModalBox toogleConfirmation={handleDelete} />
          </>
        ))}
    </>
  );
};
