import React, { useContext } from "react";
import { ProductContext } from "../../components/hooks/useProductContext";

export const AdminDashboard = () => {
  const { totals } = useContext(ProductContext);
  return (
    <>
      <div
        className="mx-auto my-5 text-left py-4 col-lg-12 col-sm-12"
        style={{ "background-color": "white" }}
      >
        <h1 className="font-weight-bolder mx-5">Overview</h1>
        <div className="mx-auto row my-5 justify-content-around align-items-center text-center">
          <div className="py-3 mb-3 econStatCard1 text-center">
            <h2>Products</h2>
            <hr />
            <h1 className="font-weight-bolder">{totals.product}</h1>
          </div>
          <div className="py-3 mb-3 econStatCard2 text-center">
            <h2>Category</h2>
            <hr />
            <h1 className="font-weight-bolder">{totals.category}</h1>
          </div>
          <div className="py-3 mb-3 econStatCard3 text-center">
            <h2>Sub Categories</h2>
            <hr />
            <h1 className="font-weight-bolder">{totals.subCategory}</h1>
          </div>
        </div>
      </div>
    </>
  );
};
