import React from "react";
import { AddOrUpdateForm } from "../../components/admin/Product/form";

export const AddProduct = (props) => {
  const handleSuccess = () =>
    props.history.push("/administration/products/view");
  return (
    <>
      <h1 className="text-center">Add New Product</h1>
      <AddOrUpdateForm onSuccess={handleSuccess} />
    </>
  );
};
