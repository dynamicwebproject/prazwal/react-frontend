import React, { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { ConfirmModalBox } from "../components/coreUI/Modal/PredefineModals";
import { CartContext } from "../components/hooks/useCartContext";
import { ProductContext } from "../components/hooks/useProductContext";
import { UserContext } from "../components/hooks/useUserContext";
import { requestPlaceOrder } from "../lib/orderApi";

const CartPage = (props) => {
  const { cartDetail, resetCartDetail } = useContext(CartContext);
  const { productList, categoryList, subCategoryList } = useContext(
    ProductContext
  );
  const { isAuthenticated, userDetail } = useContext(UserContext);

  const [costing, setCosting] = useState({ total: 0, delivery: 0, final: 0 });
  const [cartProducts, setCartProducts] = useState([]);

  const getCategory = (id) =>
    categoryList.filter((cl) => cl.id === id)[0]?.category_name;
  const getSubCategory = (id) =>
    subCategoryList.filter((scl) => scl.id === id)[0]?.subcategory_name;

  useEffect(() => {
    setCartProducts([]);
    if (cartDetail) {
      cartDetail.forEach((cd) => {
        const _each = productList.filter(
          (pl) => pl.productId === cd.productId
        )[0];
        setCartProducts((oldArray) => [...oldArray, { ..._each, ...cd }]);
      });
    }
  }, [cartDetail, productList]);

  useEffect(() => {
    if (cartProducts) {
      let _total = 0;
      let _delivery = 0;
      cartProducts.forEach((cp) => {
        if (cp.color) {
          const ele = document.getElementById(`color-${cp.productId}`);
          ele.style.backgroundColor = cp.color;
        }
        _total = _total + parseInt(cp.price);
        _delivery++;
      });
      setCosting({
        total: _total,
        delivery: _delivery * 100,
        final: _total + _delivery * 100,
      });
    }
  }, [cartProducts]);

  const handleCartClear = (val) => {
    if (val) {
      resetCartDetail();
      setCartProducts([]);
    }
  };

  const handlePlaceOrder = async () => {
    if (isAuthenticated) {
      setTimeout(() => {
        toast.success("Order placed successfully.");
        resetCartDetail();
        setCartProducts([]);
        props.history.push("/");
      }, [2000]);
    } else {
      toast.info("Please login to continue.");
    }
  };

  return (
    <>
      <div className="container row mx-auto my-5">
        {cartProducts.length > 0 ? (
          cartProducts.map((cd) => (
            <>
              <div className="col-3 p-1">
                <img
                  src={"/Images/products/doorone.jpg"}
                  className="img-fluid rounded"
                  alt="door"
                />
              </div>
              <div className="col-9 p-1">
                <button
                  type="button"
                  className="btn btn-outline float-right"
                  aria-label="Close"
                >
                  x
                </button>
                <h3>
                  {cd.name}
                  <span className="text-muted">
                    {cd.brand ? ` - ${cd.brand}` : ""}
                  </span>
                </h3>
                <h5 className="text-muted">
                  {getCategory(cd.category)}
                  {cd.sub_category
                    ? ` ( ${getSubCategory(cd.sub_category)} )`
                    : ""}
                </h5>
                <h4>
                  Color:{" "}
                  {cd.color ? (
                    <span id={`color-${cd.productId}`} className="colorSpan" />
                  ) : (
                    "default"
                  )}
                </h4>
                <h4>
                  Size: <span className="sizeSpan">{cd.size || "default"}</span>
                </h4>
                <h1>NPR {cd.price}</h1>
              </div>
            </>
          ))
        ) : (
          <div>Nothing in cart.</div>
        )}
      </div>
      <hr className="featurette-divider"></hr>
      <div className="row">
        <div className="col-12 text-center">
          <h3 className={costing.total > 0 ? "" : "text-muted"}>
            Total product cost:
            {costing.total > 0 && <span>NPR {costing.total}/-</span>}
          </h3>
          <h3 className={costing.delivery > 0 ? "" : "text-muted"}>
            Delivery charge:
            {costing.delivery > 0 && <span>NPR {costing.delivery}/-</span>}
          </h3>
          <h3 className={costing.final > 0 ? "" : "text-muted"}>
            Final cost:{costing.final > 0 && <span>NPR {costing.final}/-</span>}
          </h3>
          <hr className="featurette-divider"></hr>
          <button
            type="button"
            className="btn btn-primary mr-3"
            disabled={!cartProducts || cartProducts.length === 0}
            onClick={handlePlaceOrder}
          >
            Place order
          </button>
          <button
            type="button"
            className="btn btn-secondary ml-3"
            data-toggle="modal"
            data-target="#confirmation"
          >
            Clear cart
          </button>
        </div>
      </div>
      <ConfirmModalBox toogleConfirmation={handleCartClear} />
    </>
  );
};

export default CartPage;
