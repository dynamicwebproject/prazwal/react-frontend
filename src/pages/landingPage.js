import React, { useContext } from "react";
import { Link } from "react-router-dom";
import {
  Carousel,
  CarouselImgComponent,
} from "../components/coreUI/Carousel/index.js";
import FlexContainer from "../components/coreUI/FlexContainer/index.js";
import ProductCard from "../components/coreUI/ProductCard/index.js";
import Spinner from "../components/coreUI/Spinner/indes.js";
import Toast from "../components/coreUI/Toast/index.js";
import { ProductContext } from "../components/hooks/useProductContext.js";
import { coverImages } from "../utils/mockData";

const LandingPage = () => {
  const { productList, categoryList, subCategoryList } = useContext(
    ProductContext
  );

  const getCategory = (id) =>
    categoryList.filter((cl) => cl.id === id)[0]?.category_name;
  const getSubCategory = (id) =>
    subCategoryList.filter((scl) => scl.id === id)[0]?.subcategory_name;

  return (
    <>
      <div className="container-fluid landingCarousel">
        <Carousel autoSlide={true} totalChildren={coverImages.length}>
          {coverImages?.map((ci, i) => (
            <CarouselImgComponent
              src={ci.src}
              alt={ci.category}
              label={ci.label}
              description={ci.description}
              start={i === 0}
            />
          ))}
        </Carousel>
      </div>
      {categoryList &&
        categoryList.length > 0 &&
        categoryList.map((cat) => (
          <ProductByCat
            cat_name={cat.category_name}
            products={productList?.filter((ap) => ap.category === cat.id)}
            getCategory={getCategory}
            getSubCategory={getSubCategory}
          />
        ))}
      <Spinner />
      <Toast />
    </>
  );
};

const ProductByCat = ({
  cat_name,
  products,
  getCategory,
  getSubCategory,
}) => {
  return (
    <>
      <div className="landingProductByCat">
        <Link className="productByCatTitle" to={`/product/${cat_name}`}>
          {cat_name}
        </Link>
        <FlexContainer justify="spaceBetween" wrap={true} fill={true}>
          {products.map((p, i) => {
            if (i > 2) return;
            return (
              <ProductCard
                product={p}
                getCategory={getCategory}
                getSubCategory={getSubCategory}
                {...Object.assign({}, i === 1 ? { type: "SALE" } : null)}
              />
            );
          })}
        </FlexContainer>
        <hr className="featurette-divider"></hr>
      </div>
    </>
  );
};

export default LandingPage;
