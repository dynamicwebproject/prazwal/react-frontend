import React, { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import FlexContainer from "../../components/coreUI/FlexContainer";
import { CartContext } from "../../components/hooks/useCartContext";
import { ProductContext } from "../../components/hooks/useProductContext";
import "./styles.css";

const ProductPage = (props) => {
  const productId = props.match.params.productId;

  const { productList, getCategoryName, getSubCategoryName } = useContext(
    ProductContext
  );
  const { addToCartDetail, removeFromCartDetail, cartDetail } = useContext(
    CartContext
  );

  const [choices, setChoices] = useState({ color: "", size: "" });
  const [chosenProduct, setChosenProduct] = useState(null);
  const [addedToCart, setAddedToCard] = useState(false);

  const getColorSize = (str) =>
    str ? str.replace(/[^a-zA-Z,]/g, "").split(",") : "";

  useEffect(() => {
    if (productId && productList) {
      setChosenProduct(
        productList.filter((pl) => pl.id === parseInt(productId))[0]
      );
    }
  }, [productId, productList]);

  useEffect(() => {
    if (chosenProduct) {
      if (getColorSize(chosenProduct.color).length > 0) {
        console.log(getColorSize(chosenProduct.color));
        setChoices((prevState) => ({
          ...prevState,
          color: getColorSize(chosenProduct.color)[0],
        }));
        getColorSize(chosenProduct.color).forEach((cc) => {
          const ele = document.getElementById(`color-${cc}`);
          ele.style.backgroundColor = cc;
        });
      }
      if (chosenProduct.size) {
        setChoices((prevState) => ({
          ...prevState,
          size: getColorSize(chosenProduct.size)[0],
        }));
      }
    }
  }, [chosenProduct]);

  useEffect(() => {
    if (cartDetail.length >= 0 && chosenProduct) {
      setAddedToCard(!!cartDetail.find((cd) => cd.id === chosenProduct.id));
    }
  }, [cartDetail, chosenProduct]);

  const handleAddToCart = () => {
    addToCartDetail({
      ...choices,
      id: chosenProduct.id,
    });
    toast.success("Product added.", { autoClose: 1500 });
    setAddedToCard(true);
  };

  const handleRemoveFromCart = () => {
    toast.error("Product removed.", { autoClose: 1500 });
    removeFromCartDetail(chosenProduct.id);
  };

  return (
    <>
      {chosenProduct ? (
        <div className="row productWrapper">
          <div className="col-6 mr-1 d-flex justify-content-center align-items-center productImageWrapper">
            <img
              src={"/Images/companylogo.png"}
              alt="door"
              className="rounded img"
              width={500}
              height={500}
            />
          </div>
          <div className="col-5 ml-1 d-flex flex-column justify-content-center productContentWrapper">
            <h1 className="productName">{chosenProduct.name}</h1>
            <h3 className="productBrand">
              <label>Brand: </label>
              {chosenProduct.brand && chosenProduct.brand}
            </h3>
            <h4 className="productCategory">
              <label>Category: </label>
              {chosenProduct?.category &&
                getCategoryName(chosenProduct?.category)}
            </h4>
            <h4 className="productCategory">
              <label>Sub Category: </label>
              {chosenProduct?.sub_category &&
                getSubCategoryName(chosenProduct?.sub_category)}
            </h4>
            <div className="productColorWrapper">
              <label>Choose your color: </label>
              <FlexContainer justify="start" classlist="my-3 ml-3">
                {chosenProduct.color &&
                getColorSize(chosenProduct.color).length > 0 ? (
                  getColorSize(chosenProduct.color)?.map((color) => (
                    <div
                      className={`colorChooser ${
                        color === choices.color ? "colorChooserSelected" : ""
                      }`}
                      id={`color-${color}`}
                      onClick={() =>
                        setChoices((prevState) => ({ ...prevState, color }))
                      }
                    />
                  ))
                ) : (
                  <h5 className="text-muted">No color variant</h5>
                )}
              </FlexContainer>
            </div>
            <div className="mt-3 productSizeWrapper">
              <label>Choose you size: </label>
              <FlexContainer justify="start" classlist="my-3 ml-3">
                {chosenProduct?.size && getColorSize(chosenProduct.size) ? (
                  getColorSize(chosenProduct?.size)?.map((size) => (
                    <div
                      className={`sizeChooser ${
                        size === choices.size ? "sizeChooserSelected" : ""
                      }`}
                      onClick={() =>
                        setChoices((prevState) => ({ ...prevState, size }))
                      }
                    >
                      {size.toUpperCase()}
                    </div>
                  ))
                ) : (
                  <h5 className="text-muted">No size variant</h5>
                )}
              </FlexContainer>
            </div>
            <h1 className="my-3 productPrice">
              <span className="text-muted" style={{ "font-size": "25px" }}>
                NPR
              </span>{" "}
              {chosenProduct?.price}.00/-
            </h1>
            <label>About the product:</label>
            <p className="productDescription">
              {chosenProduct.description && chosenProduct.description}
            </p>
            {addedToCart ? (
              <button
                className="btn btn-outline-danger w-25"
                onClick={handleRemoveFromCart}
              >
                Remove from cart
              </button>
            ) : (
              <button
                className="btn btn-primary w-25"
                onClick={handleAddToCart}
              >
                Add to cart
              </button>
            )}
          </div>
        </div>
      ) : (
        <div>No Product</div>
      )}
    </>
  );
};

export default ProductPage;
