import React, { useContext, useEffect, useState } from "react";
import FlexContainer from "../components/coreUI/FlexContainer";
import ProductCard from "../components/coreUI/ProductCard";
import { ProductContext } from "../components/hooks/useProductContext";

const ProductByCategoryPage = (props) => {
  const category = props.match.params.category;
  const { productList, categoryList, subCategoryList } = useContext(
    ProductContext
  );

  const [products, setProducts] = useState([]);

  useEffect(() => {
    if (categoryList.length > 0) {
      const categoryId = categoryList.filter(
        (cat) => cat.category_name.toLowerCase() === category.toLowerCase()
      )[0].id;
      console.log(categoryId);
      setProducts(productList.filter((pl) => pl.category === categoryId));
    }
  }, [category]);

  const getCategory = (id) =>
    categoryList.filter((cl) => cl.id === id)[0]?.category_name;
  const getSubCategory = (id) =>
    subCategoryList.filter((scl) => scl.id === id)[0]?.subcategory_name;

  return (
    <div className="categoryPageWrapper">
      <h1 className="mt-5">{category.toUpperCase()}<span className="text-muted">s</span></h1>
      <FlexContainer justify="spaceBetween" classlist="productWrapper">
        {products.map((ap, i) => (
          <ProductCard
            product={ap}
            getCategory={getCategory}
            getSubCategory={getSubCategory}
            {...Object.assign({}, i % 2 === 1 ? { type: "SALE" } : null)}
          />
        ))}
      </FlexContainer>
    </div>
  );
};

export default ProductByCategoryPage;
