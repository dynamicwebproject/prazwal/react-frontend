import React from "react";
import AppRouting from "./route";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import UserProvider from "./components/hooks/useUserContext";
import CartProvider from "./components/hooks/useCartContext";
import ProductProvider from "./components/hooks/useProductContext";

//CSS
import "./App.css";
import "./pages/styles.css";
import "react-toastify/dist/ReactToastify.css";
function App() {
  return (
   <div>
      <BrowserRouter>
        <ToastContainer autoClose={3000} />
        <UserProvider>
          <ProductProvider>
            <CartProvider>
              <AppRouting />
            </CartProvider>
          </ProductProvider>
        </UserProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
