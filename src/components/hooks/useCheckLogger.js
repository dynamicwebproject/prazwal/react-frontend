import CryptoJS from "crypto-js";
import { crypto_secret } from "../../utils/constants";

export const useCheckLogger = () => {
  const decrypto = (cyperText) => {
    return CryptoJS.AES.decrypt(cyperText, crypto_secret).toString(
      CryptoJS.enc.Utf8
    );
  };

  const logger = decrypto(localStorage.getItem("logger") || "");

  const isAdmin = () => logger.toUpperCase() === "TRUE";

  return {
    isAdmin: isAdmin(),
    decrypto,
  };
};
