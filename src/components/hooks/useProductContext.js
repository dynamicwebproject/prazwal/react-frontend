import React, { createContext, useEffect, useState } from "react";
import {
  requestGetAllCategories,
  requestGetAllSubCategories,
} from "../../lib/categoryApi";
import { requestGetAllProducts } from "../../lib/productsApi";

export const ProductContext = createContext();

const ProductProvider = ({ children }) => {
  const [productList, updateProductList] = useState([]);
  const [categoryList, updateCategoryList] = useState([]);
  const [subCategoryList, updateSubCategoryList] = useState([]);
  const [totals, setTotals] = useState({
    product: 0,
    category: 0,
    subCategory: 0,
  });

  const setProductList = async () => {
    const res = await requestGetAllProducts();
    if (res && !res.error) {
      updateProductList(res);
      setTotals((prevState) => ({ ...prevState, product: res.length || 0 }));
    }
  };

  const setCategoryList = async () => {
    const res = await requestGetAllCategories();
    if (res && !res.error) {
      updateCategoryList(res);
      setTotals((prevState) => ({ ...prevState, category: res.length || 0 }));
    }
  };

  const setSubCategoryList = async () => {
    const res = await requestGetAllSubCategories();
    if (res && !res.error) {
      updateSubCategoryList(res);
      setTotals((prevState) => ({
        ...prevState,
        subCategory: res.length || 0,
      }));
    }
  };

  const getCategoryName = (id) =>
    categoryList.filter((cl) => cl.id === id)[0]?.category_name;
  const getSubCategoryName = (id) =>
    subCategoryList.filter((scl) => scl.id === id)[0]?.subcategory_name;

  useEffect(() => {
    setProductList();
    setCategoryList();
    setSubCategoryList();
  }, []);

  return (
    <ProductContext.Provider
      value={{
        productList,
        categoryList,
        subCategoryList,
        setProductList,
        setCategoryList,
        setSubCategoryList,
        getCategoryName,
        getSubCategoryName,
        totals,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};

export default ProductProvider;
