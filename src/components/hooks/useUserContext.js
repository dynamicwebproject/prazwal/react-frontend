import React, { createContext, useEffect, useState } from "react";
import { getUserDetail } from "../../lib/user";

export const UserContext = createContext();

const UserProvider = ({ children }) => {
  const [userDetail, updateUserDetail] = useState(null);

  const setUserDetail = async () => {
    const res = await getUserDetail();
    if (res && !res.error) {
      updateUserDetail(res);
    }
  };

  useEffect(() => {
    setUserDetail();
  }, []);

  return (
    <UserContext.Provider
      value={{ userDetail, setUserDetail, isAuthenticated: !!userDetail }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
