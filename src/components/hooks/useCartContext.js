import React, { createContext, useEffect, useState } from "react";

export const CartContext = createContext();

const CartProvider = ({ children }) => {
  const [cartDetail, updateCartDetail] = useState([]);

  useEffect(() => {
    const localData = localStorage.getItem("cart");
    if (localData) {
      updateCartDetail(JSON.parse(localData));
    }
  }, []);

  const setCartDetail = (updatedDetail) => {
    localStorage.setItem("cart", JSON.stringify(updatedDetail));
    updateCartDetail(updatedDetail);
  };

  const removeFromCartDetail = (id) => {
    const updatedDetail = cartDetail.filter((cd) => cd.id !== id);
    setCartDetail(updatedDetail);
  };

  const addToCartDetail = (detail) => {
    const updatedDetail = [...cartDetail, detail];
    setCartDetail(updatedDetail);
  };

  const resetCartDetail = () => {
    localStorage.removeItem("cart");
    updateCartDetail([]);
  };

  return (
    <CartContext.Provider
      value={{
        cartDetail,
        removeFromCartDetail,
        addToCartDetail,
        resetCartDetail,
        totalInCart: cartDetail.length,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export default CartProvider;
