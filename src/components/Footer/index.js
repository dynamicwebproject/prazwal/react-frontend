import React from "react";
import "./styles.css";

const Footer = () => {
  return (
    <div className="footerWrapper container-fluid">
      <div className="card">
        <div className="card-body">This is some text within a card body.</div>
      </div>
    </div>
  );
};

export default Footer;
