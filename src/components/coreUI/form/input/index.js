import React from "react";
import "./style.css";

export const Input = ({
  handleChange,
  error,
  ref,
  value,
  id,
  placeholder,
  name,
  label,
  type,
  onBlur,
  autoFocus,
  required,
  title,
  info,
}) => {
  return (
    <div className="inputWrapper">
      {label && (
        <label htmlFor={id} className="inputLabel">
          {label}
        </label>
      )}
      {error && <span className="inputError">{error}</span>}
      {!error && info && <span className="inputInfo">{info}</span>}

      <input
        type={type}
        id={id}
        name={name}
        value={value}
        onChange={handleChange}
        ref={ref}
        placeholder={placeholder}
        onBlur={onBlur}
        autoFocus={autoFocus}
        required={required}
        className="inputElement"
        title={title}
      />
    </div>
  );
};

export const Textarea = ({
  handleChange,
  error,
  ref,
  value,
  id,
  placeholder,
  name,
  label,
  onBlur,
  autoFocus,
  required,
}) => {
  return (
    <div className="inputWrapper">
      {label && (
        <label htmlFor={id} className="inputLabel">
          {label}
        </label>
      )}
      {error && <span className="inputError">{error}</span>}

      <textarea
        id={id}
        name={name}
        value={value}
        onChange={handleChange}
        ref={ref}
        placeholder={placeholder}
        onBlur={onBlur}
        autoFocus={autoFocus}
        required={required}
        className="inputElement textAreaElement"
      />
    </div>
  );
};

export const SelectOption = ({
  handleChange,
  error,
  ref,
  value,
  id,
  placeholder,
  name,
  label,
  options,
  onBlur,
  autoFocus,
  required,
  wrapperClass,
  disabled,
}) => {
  return (
    <div className={`inputWrapper ${wrapperClass}`}>
      {label && (
        <label htmlFor={id} className="inputLabel">
          {label}
        </label>
      )}
      {error && <span className="inputError">{error}</span>}
      <select
        id={id}
        name={name}
        value={value}
        onChange={handleChange}
        ref={ref}
        placeholder={placeholder}
        onBlur={onBlur}
        disabled={disabled}
        autoFocus={autoFocus}
        required={required}
        className="inputElement"
      >
        <option value="" disabled selected>
          --- Select {placeholder} ---
        </option>
        {options &&
          options.length > 0 &&
          options.map((opt) => (
            <option key={opt.key} value={opt.value}>
              {opt.displayName}
            </option>
          ))}
      </select>
    </div>
  );
};
