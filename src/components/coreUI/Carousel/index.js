import React from "react";

import "./styles.css";

const Carousel = ({ autoSlide, totalChildren, children }) => {
  let ele = [];
  for (let i = 0; i < totalChildren; i++) {
    ele.push(i);
  }
  return (
    <div
      id="carouselExampleIndicators"
      className={`carousel ${autoSlide ? "slide" : ""} carouselWrapper`}
      data-ride="carousel"
    >
      <ol className="carousel-indicators">
        {ele.map((e, i) => (
          <li
            data-target="#"
            data-slide-to={i + 1}
            className={i === 0 ? "active" : ""}
          />
        ))}
      </ol>
      <div className="carousel-inner">{children}</div>
      <a
        className="carousel-control-prev"
        href="#carouselExampleIndicators"
        role="button"
        data-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="sr-only">Previous</span>
      </a>
      <a
        className="carousel-control-next"
        href="#carouselExampleIndicators"
        role="button"
        data-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </a>
    </div>
  );
};

const CarouselImgComponent = ({ src, alt, label, description, start }) => {
  return (
    <div className={`carousel-item ${start ? "active" : ""} carouselImgWraper`}>
      <img className="d-block w-100 h-100" src={src} alt={alt} />
      {label && (
        <div className="carousel-caption d-none d-md-block carouselLabelWrapper">
          <h3 className="carouselLabelHead">{label}</h3>
          {description && <p className="carouselLabelPara">{description}</p>}
        </div>
      )}
    </div>
  );
};

export { Carousel, CarouselImgComponent };
