import React, { forwardRef } from "react";

import "./styles.css";

const FlexContainer = forwardRef((props, ref) => {
  const {
    direction = "row",
    fill,
    classlist,
    justify,
    align,
    wrap = false,
    children,
  } = props;
  const classes = `${direction || ""} ${fill ? "fill" : ""} ${
    classlist || ""
  } ${justify || ""} ${align || ""} ${wrap ? "wrap" : ""}`;
  return (
    <div ref={ref} className={classes}>
      {children}
    </div>
  );
});

export default FlexContainer;
