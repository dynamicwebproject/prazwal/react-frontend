import React from "react";

const Spinner = () => {
  return (
    <div className="spinner-border text-primary" role="status">
      <span className="visually-hidden" />
    </div>
  );
};

export default Spinner;
