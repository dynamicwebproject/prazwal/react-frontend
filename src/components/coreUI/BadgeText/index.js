import React from "react";

const BadgeText = ({ type = "" }) => {
  switch (type.toUpperCase()) {
    case "NEW":
      return <span className="badge bg-danger text-white mx-3">NEW</span>;
    case "SALE":
      return <span className="badge bg-primary text-white mx-3">SALE</span>;
    case "OUT":
      return <span className="badge bg-secondary text-white mx-3">OUT</span>;
    default:
      return null;
  }
};

export default BadgeText;