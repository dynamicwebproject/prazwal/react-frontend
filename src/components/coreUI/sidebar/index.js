import React from "react";
import { NavLink } from "react-router-dom";
import { adminNavigations } from "../../../utils/constants";
import FlexContainer from "../FlexContainer/index";
import { LogoutModalBox } from "../Modal/PredefineModals";
import "./styles.css";

const AdminSideNavigation = () => {
  return (
    <>
      <div className="menuWrapper">
        <FlexContainer
          classlist="menuNav d-flex"
          direction="col"
          justify="spaceBetween"
          align="center"
        >
          <ul>
            {adminNavigations.map((na, i) => (
              <li
                className={`nav-item ${na.subNavs ? "dropdown" : ""}`}
                key={i}
              >
                {na.subNavs ? (
                  <>
                    <a className="nav-link dropdown-toggle" role="button">
                      {na.displayName}
                    </a>
                    {na.subNavs.map((sna) => (
                      <NavLink
                        exact
                        to={sna.url}
                        className="nav-link subNavs"
                        activeClassName="activeNav"
                      >
                        {sna.displayName}
                      </NavLink>
                    ))}
                  </>
                ) : (
                  <NavLink
                    exact
                    to={na.url}
                    className="nav-link"
                    activeClassName="activeNav"
                  >
                    {na.displayName}
                  </NavLink>
                )}
              </li>
            ))}
          </ul>
          <div>
            <button
              className="btn btn-lg btn-danger w-100 mb-5"
              data-toggle="modal"
              data-target="#logoutModal"
            >
              Logout
            </button>
          </div>
        </FlexContainer>
      </div>
    </>
  );
};

export default AdminSideNavigation;
