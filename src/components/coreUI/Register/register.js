import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { requestAddUser, requestUpdateUser } from "../../../lib/customerApi";
import { Input } from "../../coreUI";

const initialFormState = {
  first_name: "",
  last_name: "",
  email: "",
  username: "",
  password: "",
};

export const RegisterForm = ({ updateData }) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialFormState);

  let isUpdate = !!updateData;

  useEffect(() => {
    isUpdate = !!updateData;
    if (isUpdate) {
      Object.keys(formState).map((key) => {
        setFormState((prevState) => ({
          ...prevState,
          [key]: updateData[key] || formState[key],
        }));
      });
    } else {
      setFormState(initialFormState);
    }
  }, [updateData]);

  const closeModal = () => {
    setFormState(initialFormState);
    setErrorState(initialFormState);
    document.getElementById("modalCloser").click();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      let res;
      if (isUpdate) {
        res = await requestUpdateUser(formState, updateData.id);
      } else {
        res = await requestAddUser(formState);
      }
      if (!res || res.error) {
        toast.error(res.error);
      } else {
        toast.success("Successful");
        closeModal();
      }
    }
  };

  const validate = () => {
    let _errors = {};
    if (!formState.first_name) _errors.first_name = "First name is required.";
    if (!formState.last_name) _errors.last_name = "Last name is required.";
    if (!formState.email) _errors.email = "Email Address is required.";
    if (!formState.username) _errors.username = "Username is required.";
    if (!formState.password) _errors.password = "Password is required.";
    setErrorState(_errors);
    return Object.keys(_errors).length === 0;
  };

  return (
    <form onSubmit={handleSubmit}>
      <Input
        type="text"
        name="first_name"
        placeholder="First Name"
        value={formState.first_name}
        error={errorState.first_name}
        handleChange={handleChange}
      />
      <Input
        type="text"
        name="last_name"
        placeholder="Last Name"
        value={formState.last_name}
        error={errorState.last_name}
        handleChange={handleChange}
      />
      <Input
        type="text"
        name="email"
        placeholder="Email Address"
        value={formState.email}
        error={errorState.email}
        readOnly={isUpdate}
        handleChange={handleChange}
      />
      <Input
        type="text"
        name="username"
        placeholder="Username"
        value={formState.username}
        error={errorState.username}
        readOnly={isUpdate}
        handleChange={handleChange}
      />
      <Input
        type="password"
        name="password"
        placeholder="Password"
        value={formState.password}
        error={errorState.password}
        readOnly={isUpdate}
        handleChange={handleChange}
      />
      <button
        className="btn btn-primary"
        type="submit"
        disabled={() => validate()}
      >
        {isUpdate ? "Update Profile" : "Register"}
      </button>
    </form>
  );
};
