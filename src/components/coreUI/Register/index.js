import React, { useState } from "react";
import ModalBox from "../Modal";
import { RegisterForm } from "./register";

const Register = () => {
  const [refresh, setRefresh] = useState(false);
  return (
    <>
      <button
        className="btn btn-sm btn-outline-primary mx-2"
        type="button"
        data-toggle="modal"
        data-target="#registerModal"
      >
        Register
      </button>
      <ModalBox
        id="registerModal"
        title="Register your account."
        onCloseClick={() => setRefresh(!refresh)}
        isStatic={false}
      >
        <RegisterForm refresh={refresh} />
      </ModalBox>
    </>
  );
};

export default Register;
