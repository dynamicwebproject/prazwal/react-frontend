import React, { useContext, useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { navigations } from "../../../utils/constants";
import { CartContext } from "../../hooks/useCartContext";
import { UserContext } from "../../hooks/useUserContext";
import { ProductContext } from "../../hooks/useProductContext";
import Login from "../Login";
import { LogoutModalBox } from "../Modal/PredefineModals";
import Register from "../Register";
import "./styles.css";

const NavBar = () => {
  const { isAuthenticated } = useContext(UserContext);
  const { totalInCart } = useContext(CartContext);
  const { categoryList } = useContext(ProductContext);

  const [updatedNav, setUpdatedNav] = useState([]);

  useEffect(() => {
    setUpdatedNav([]);
    let toUpdate = [...navigations];
    if (categoryList && categoryList.length > 0) {
      categoryList.forEach((cat) => {
        toUpdate[3].subNavs.push({
          url: `/product/${cat.category_name}`,
          displayName: cat.category_name,
        });
      });
    }
    setUpdatedNav(toUpdate);
  }, [categoryList]);

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      {!isAuthenticated ? (
        <>
          <Login />
          <Register />
        </>
      ) : (
        <div className="btn btn-sm btn-warning mr-5">Logged In</div>
      )}
      <NavLink className="navbar-brand" to="/">
        R n B TRADE
      </NavLink>

      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav m-auto">
          {updatedNav.length > 0 &&
            updatedNav.map((na) => {
              return (
                <li
                  className={`nav-item mr-4 ml-4 ${
                    na.subNavs ? "dropdown" : ""
                  }`}
                >
                  {na.subNavs ? (
                    <>
                      <a
                        className="nav-link dropdown-toggle"
                        href="#"
                        id="navbarDropdown"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        {na.displayName}
                      </a>
                      <div
                        className="dropdown-menu"
                        aria-labelledby="navbarDropdown"
                      >
                        {na.subNavs.map((sna) => (
                          <NavLink
                            exact
                            to={sna.url}
                            className="dropdown-item"
                            activeClassName="active"
                          >
                            {sna.displayName}
                          </NavLink>
                        ))}
                      </div>
                    </>
                  ) : (
                    <NavLink
                      exact
                      to={na.url}
                      className="nav-link"
                      activeClassName="active"
                    >
                      {na.displayName}
                    </NavLink>
                  )}
                </li>
              );
            })}
        </ul>

        {/* <form className="d-flex my-2 my-lg-0">
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
          />
          <button
            className="btn btn-sm btn-outline-info my-2 my-sm-0"
            type="submit"
          >
            Search
          </button>
        </form> */}
        <Link
          to="/cart"
          role="button"
          className="btn btn-sm btn-outline-success mx-4"
        >
          <i className="bi bi-cart"></i> Cart <span>{totalInCart}</span>
        </Link>
        {isAuthenticated && (
          <>
            <button
              className="btn btn-sm btn-outline-danger"
              type="button"
              data-toggle="modal"
              data-target="#logoutModal"
            >
              <i className="bi bi-box-arrow-right"></i> Logout
            </button>
            <LogoutModalBox />
          </>
        )}
      </div>
    </nav>
  );
};

export default NavBar;
