import React from "react";
import { Link } from "react-router-dom";
import BadgeText from "../BadgeText";
import "./style.css";

const ProductCard = ({ product, getCategory, getSubCategory, type }) => {
  return (
    <Link
      to={`/product/${product.category}/${product.id}`}
      className="card productCardWrapper"
    >
      <img
        src={product.image || "/Images/companylogo.png"}
        className="card-img-top"
        alt={product.name}
      />
      <div className="productNames">
        <h5>
          {getCategory(product.category)} ({" "}
          {getSubCategory(product.sub_category)} )
        </h5>
        <p className="card-text">{product.description}</p>
      </div>

      <div className="card-body">
        <h2>{product?.name?.toUpperCase()}</h2>
        <h4>{product.brand}</h4>
        <h1 className="card-title">
          NPR {product.price}.00/-
          <BadgeText type={type} />
        </h1>
      </div>
    </Link>
  );
};

export default ProductCard;
