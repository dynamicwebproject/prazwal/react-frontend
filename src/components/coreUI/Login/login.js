import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Input } from "..";
import { requestLogin } from "../../../lib/authApi";
import { successes } from "../../../utils/constants";
import FlexContainer from "../FlexContainer";

import "./styles.css";

const initialFormState = {
  username: "",
  password: "",
};

const initialErrorState = {
  username: "",
  password: "",
};

const LoginForm = ({ refresh }) => {
  const [formState, setFormState] = useState(initialFormState);
  const [errorState, setErrorState] = useState(initialErrorState);

  useEffect(() => {
    setFormState(initialFormState);
    setErrorState(initialErrorState);
  }, [refresh]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
    setErrorState(initialErrorState);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await requestLogin({
      username: formState.username,
      password: formState.password,
    });
    if (!response || response.error) {
      setErrorState({ username: response.error, password: response.error });
    } else {
      toast.success(successes.auth.login);
      window.location.reload();
    }
  };

  const disableSubmitButton = () =>
    !formState.password ||
    !formState.username ||
    errorState.password ||
    errorState.username;

  return (
    <form onSubmit={handleSubmit} className="loginForm">
      <Input
        autoFocus={true}
        type="text"
        name="username"
        handleChange={handleChange}
        value={formState.username}
        error={errorState.username}
        placeholder="Username"
        label="Username"
      />
      <Input
        type="password"
        name="password"
        handleChange={handleChange}
        value={formState.password}
        error={errorState.password}
        placeholder="Password"
        label="Password"
      />
      <FlexContainer direction="row" justify="spaceEven" fill={true}>
        <button
          type="submit"
          className="btn btn-primary"
          disabled={disableSubmitButton()}
        >
          Login
        </button>
        <button
          type="button"
          className="btn btn-danger"
        >
          Cancel
        </button>
      </FlexContainer>
    </form>
  );
};

export default LoginForm;
