import React, { useState } from "react";
import { Input } from "../form/input";
import ModalBox from "../Modal";
import LoginForm from "./login";

const Login = () => {
  const [refresh, setRefresh] = useState(false);
  return (
    <>
      <button
        className="btn btn-sm btn-primary"
        type="button"
        data-toggle="modal"
        data-target="#loginModal"
      >
        <i className="bi bi-box-arrow-in-left"></i> Login
      </button>
      <ModalBox
        id="loginModal"
        title="Login your account."
        onCloseClick={() => setRefresh(!refresh)}
        isStatic={true}
      >
        <LoginForm refresh={refresh} />
      </ModalBox>
    </>
  );
};

export default Login;
