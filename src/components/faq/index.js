import React from "react";
import { faquestions } from "../../utils/constants";

import "./styles.css";

const FAQ = () => {
  return (
    <>
      <h1 className="pageHeadings">Frequently asked questions</h1>
      {faquestions.map((faq, i) => (
        <div className="faqWrapper">
          {i !== 0 && <hr className="featurette-divider" />}
          <h3>{faq.question}</h3>
          <p>{faq.answer}</p>
        </div>
      ))}
    </>
  );
};

export default FAQ;
