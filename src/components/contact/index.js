import React from "react";
import { Input, Textarea } from "../../components/coreUI/form/input/index";

import "./styles.css";

const Contact = () => {
  return (
    <>
      <h1 className="pageHeadings">Contact Us</h1>
      <div className="row directMessageWrapper">
        <div className="col-6 justify-content-center">
          <form className=" directMessageForm">
            <Input
              type="text"
              name="fullname"
              className="form-control"
              placeholder="Full name"
            />
            <Input
              type="number"
              name="phone"
              className="form-control"
              placeholder="Phone number"
            />
            <Input
              type="email"
              name="email"
              className="form-control"
              placeholder="Email address"
            />
            <Input
              type="text"
              name="subject"
              className="form-control"
              placeholder="Subject"
            />
            <Textarea
              name="detail"
              className="inputElement"
              placeholder="In detail..."
            />
            <button type="submit" className="btn btn-primary">
              Send Message
            </button>
          </form>
        </div>
        <div className="col-6">
          <ol>
            <li>
              <h3>Call us at:</h3>
              <ul>
                <li>1234567890</li>
                <li>1234567890</li>
                <li>1234567890</li>
              </ul>
            </li>
            <li>
              <h3>Email at:</h3>
              <ul>
                <li>abc@gmail.com</li>
                <li>abc@gmail.com</li>
              </ul>
            </li>
            <li>
              <h3>Visit us:</h3>
            </li>
          </ol>
          <div>Location</div>
        </div>
      </div>
    </>
  );
};

export default Contact;
