import React from "react";

export const TotalBox = ({ title, total }) => {
  return (
    <div className="totalBoxWrapper">
      <h3 className="totalBoxTitle">{title}</h3>
      <h1 className="totalBoxScore">{total}</h1>
    </div>
  );
};
