import React, { useContext } from "react";
import { groupBy } from "lodash";
import { Link } from "react-router-dom";
import { ProductContext } from "../../hooks/useProductContext";

export const ProductList = () => {
  const { productList, categoryList } = useContext(ProductContext);
  const groupByCategory = groupBy(productList, "category");

  return (
    <>
      {categoryList &&
        categoryList.length > 0 &&
        categoryList.map((cat) => (
          <>
            <h2>{cat.category_name.toUpperCase()}</h2>
            <table
              className="table m-0"
              style={{ "background-color": "white" }}
            >
              <thead className="thead-dark">
                <tr>
                  <th scope="col">Image</th>
                  <th scope="col">Name</th>
                  <th scope="col" className="mw-50">
                    Status
                  </th>
                  <th scope="col">Description</th>
                  <th scope="col" className="mw-50">
                    Quantity
                  </th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                {groupByCategory &&
                  groupByCategory[cat.id] &&
                  groupByCategory[cat.id].length > 0 &&
                  groupByCategory[cat.id].map((val) => (
                    <tr>
                      <td>
                        <img
                          src={val.image || "/Images/companylogo.png"}
                          alt={val.name}
                          height="50px"
                          width="50px"
                          className="img-fluid"
                          style={{ height: "50px", width: "50px" }}
                        />
                      </td>
                      <td>{val.name}</td>
                      <td>{val.status}</td>
                      <td className="econTableDesc">{val.description}</td>
                      <td>{val.quantity}</td>
                      <td>
                        <Link
                          role="button"
                          className="btn btn-sm btn-outline-info"
                          to={`/administration/products/update/${val.id}`}
                        >
                          Edit
                        </Link>
                        <button
                          type="button"
                          className="btn btn-sm btn-outline-danger"
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </>
        ))}
    </>
  );
};
