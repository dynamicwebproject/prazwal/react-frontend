import React, { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
  requestAddProduct,
  requestUpdateProduct,
} from "../../../lib/productsApi";
import { errors } from "../../../utils/constants";
import FlexContainer from "../../coreUI/FlexContainer/index";
import { Input, SelectOption, Textarea } from "../../coreUI/form/input/index";
import { ProductContext } from "../../hooks/useProductContext";

const initialFormState = {
  name: "",
  description: "",
  image: "",
  category: "",
  sub_category: "",
  quantity: 0,
  color: "",
  size: "",
  brand: "",
  price: 0,
};

export const AddOrUpdateForm = ({ updateData, onSuccess }) => {
  const { categoryList, subCategoryList, setProductList } = useContext(
    ProductContext
  );
  const [formstate, setFormState] = useState(initialFormState);

  const isUpdate = !!updateData;

  useEffect(() => {
    if (isUpdate) {
      Object.keys(formstate).forEach((key) => {
        setFormState((prevState) => ({
          ...prevState,
          [key]: updateData[key] || formstate[key],
        }));
      });
    }
  }, [updateData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let res;
    if (isUpdate) {
      res = await requestUpdateProduct(formstate, updateData.id);
    } else {
      res = await requestAddProduct(formstate);
    }
    if (!res || res.error) {
      toast.error(res?.error || errors.generic);
    } else {
      toast.success("Operation successful");
      setFormState(initialFormState);
      setProductList();
      onSuccess();
    }
  };

  const selectCatOptions = categoryList.map((cat) => {
    return {
      key: cat.id,
      value: cat.id,
      displayName: cat.category_name,
    };
  });

  const selectSubCatOptions = subCategoryList
    .filter((scl) => scl.category === parseInt(formstate.category))
    .map((subCat) => {
      return {
        key: subCat.id,
        value: subCat.id,
        displayName: subCat.subcategory_name,
      };
    });

  return (
    <>
      <FlexContainer justify="spaceEven" fill={true}>
        <form>
          <SelectOption
            name="category"
            id="category"
            value={formstate.category}
            handleChange={handleChange}
            required={true}
            placeholder="Category"
            options={selectCatOptions}
            label="Category"
          />
          <SelectOption
            name="sub_category"
            id="sub_category"
            value={formstate.sub_category}
            handleChange={handleChange}
            required={true}
            disabled={!formstate.category}
            placeholder="Sub Category"
            options={selectSubCatOptions}
            label="Sub Category"
          />
          <Input
            type="text"
            id="productName"
            name="name"
            value={formstate.name}
            handleChange={handleChange}
            placeholder="Product name"
            label="Name"
            required={true}
          />
          <Textarea
            id="productDescription"
            name="description"
            value={formstate.description}
            handleChange={handleChange}
            placeholder="Product Description"
            label="Description"
            required={true}
          />
        </form>
        <form>
          <Input
            type="text"
            id="productSizes"
            name="size"
            value={formstate.size}
            handleChange={handleChange}
            placeholder="Product sizes"
            label="Sizes"
            required={true}
            info="Please separate using coma ','"
          />
          <Input
            type="text"
            id="productColors"
            name="color"
            value={formstate.color}
            handleChange={handleChange}
            placeholder="Product colors"
            label="colors"
            required={true}
            info="Please separate using coma ','"
          />
          <Input
            type="number"
            id="productQuantity"
            name="quantity"
            value={formstate.quantity}
            handleChange={handleChange}
            placeholder="Quantity"
            label="Quantity"
            required={true}
          />
          <Input
            type="text"
            id="productBrand"
            name="brand"
            value={formstate.brand}
            handleChange={handleChange}
            placeholder="Product brand"
            label="Brand"
            required={true}
          />
          <Input
            type="number"
            id="productPrice"
            name="price"
            value={formstate.price}
            handleChange={handleChange}
            placeholder="Price"
            label="Price"
            required={true}
          />
        </form>
      </FlexContainer>
      <input
        type="button"
        className="btn btn-primary w-25 d-block mx-auto mt-2"
        value={`${isUpdate ? "Update" : "Add new"} product`}
        onClick={handleSubmit}
      />
    </>
  );
};
