import React, { useContext, useEffect, useState } from "react";
import { Input, SelectOption, Textarea } from "../../coreUI/form/input/index";
import FlexContainer from "../../coreUI/FlexContainer/index";
import { ProductContext } from "../../hooks/useProductContext";

const initialFormState = {
  name: "",
  description: "",
  img: "",
  category: "",
  quantity: 0,
  color: "",
  size: "",
};

export const AddUpdateProduct = (props) => {
  const { productList, categoryList } = useContext(ProductContext);
  const paramsProductId = props.match.params.productId;
  const [formstate, setFormState] = useState(initialFormState);

  useEffect(() => {
    if (paramsProductId) {
      const productDetail = productList.filter(
        (pro) => pro.id === paramsProductId
      )[0];
      console.log(productDetail);
      Object.keys(formstate).forEach((key) => {
        setFormState((prevState) => ({
          ...prevState,
          [key]: productDetail[key],
        }));
      });
    }
  }, [paramsProductId]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    console.log(name);
    setFormState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formstate);
  };

  const selectOptions = categoryList.map((cat) => {
    return {
      key: cat.id,
      value: cat.id,
      displayName: cat.category_name,
    };
  });

  return (
    <>
      <h1 style={{ "text-align": "center", margin: "30px 0" }}>
        Add New Product
      </h1>
      <FlexContainer justify="spaceEven" fill={true}>
        <form onSubmit={handleSubmit}>
          <SelectOption
            name="category"
            id="category"
            value={formstate.category}
            handleChange={handleChange}
            required={true}
            placeholder="Category"
            options={selectOptions}
            label="Category"
          />
          <Input
            type="text"
            id="productName"
            name="name"
            value={formstate.name}
            handleChange={handleChange}
            placeholder="Product name"
            label="Name"
            required={true}
          />
          <Textarea
            id="productDescription"
            name="description"
            value={formstate.description}
            onChange={handleChange}
            placeholder="Product Description"
            label="Description"
            required={true}
          />
        </form>
        <form>
          <Input
            type="text"
            id="productSizes"
            name="size"
            value={formstate.size}
            handleChange={handleChange}
            placeholder="Product sizes"
            label="Sizes"
            required={true}
            info="Please separate using coma ','"
          />
          <Input
            type="text"
            id="productColors"
            name="color"
            value={formstate.color}
            handleChange={handleChange}
            placeholder="Product colors"
            label="colors"
            required={true}
            info="Please separate using coma ','"
          />
          <Input
            type="number"
            id="productQuantity"
            name="quantity"
            value={formstate.quantity}
            handleChange={handleChange}
            placeholder="Quantity"
            label="Quantity"
            required={true}
          />
          <Input
            type="file"
            label="Product Image"
            id="productImage"
            name="img"
            required={true}
            accept="image/*"
          />
        </form>
      </FlexContainer>
      <input
        type="submit"
        className="btn btn-primary w-25 d-block mx-auto mt-5"
        value="Add Product"
      />
    </>
  );
};
